= Ein Croissant am Morgen
Mathias-H. Weber
:lang: de
:sectlinks:
// Makro "kbd:" aktivieren:
:experimental:

== Neujahrsempfang 2017

*Herzlich willkommen!*

Dieses Jahr hatte ich zwischen den Jahren noch weniger Zeit als im vergangenen, so etwas wie eine Rede vorzubereiten!

Da aber in der Einladung zur heutigen Veranstaltung von einer Rede die Rede war, komme ich ja nicht umhin, hier einige Worte zum besten zu geben.



Ich will euch daher heute mal eine echte Abenteuergeschichte erzählen. Im Alltag und auf dem Wasser haben die Frauen uns Männern ja längst den Rang abgelaufen.


Aber was Abenteuer angeht -- da gibt es noch welche, die können nur Männer erleben -- nur _echte_ Männer! 


Wer schon einmal Samstags morgens beim Bäcker war -- der weiß, dass wir Männer da unter uns sind -- der weiß, wovon ich rede!

[quote, Philippe Delerm, Ein Croissant am Morgen ]
____
Du bist als Erster aufgewacht.
Vorsichtig wie ein indianischer Späher
hast du dich angezogen,
bist dann von Zimmer zu Zimmer geschlichen.
Du hast die Eingangstür geöffnet
und zugezogen,
mit uhrmacherhafter Gewissenhaftigkeit.

So. Jetzt bist du draußen,
im Blau des rosa gesäumten Morgens.
Eine Kitschhochzeit, wäre da nicht die Kälte,
die alles klärt.
Mit jedem Atemzug bläst du eine Dampfwolke aus:
Du lebst,
frei und leicht auf dem frühmorgendlichen Bürgersteig.
Dass die Bäckerei etwas weiter weg ist,
umso besser.
Die Hände in den Taschen,
lässig wie Kerouac,
bist du allen zuvorgekommen:
Jeder Schritt ist ein Fest.
Du ertappst dich dabei,
dass du ganz außen am Bürgersteig gehst,
wie als Kind,
als wäre die Kante wichtig,
der Rand der Dinge.
Zeit in Reinform ist das,
dieses Stromern,
das du dem Tag stibitzt,
während alle anderen noch schlafen.

Fast alle.

Dort hinten muss freilich das warme Licht der Bäckerei warten
- eigentlich eine Neonleuchte,
aber die Vorstellung von Wärme verleiht
ihr einen bernsteingelben Schimmer.
Auch muss das Schaufenster ordentlich beschlagen sein,
während du näher kommst,
und du erwartest das heitere "Guten Morgen",
das die Bäckerin nur für die allerersten Kunden bereithält
- Dämmerungs-Gemeinschaft.

"Fünf Croissants, ein Kastenweißbrot,
 aber nicht zu stark gebacken!"

Der Bäcker in seinem mehlbestäubten Trägerhemdchen
zeigt sich hinten im Laden und grüßt dich,
wie man die Tapferen grüßt in der Stunde des Kampfes.

Dann bist du wieder auf der Straße.
Du spürst, der Rückweg ist nicht mehr dasselbe.
Der Bürgersteig ist voller,
ein bisschen bürgerlicher
wegen dieses unter den Arm geklemmten Weißbrots,
der Croissant-Tüte in der anderen Hand.
Aber du nimmst ein Croissant heraus.
Der Teig ist lauwarm, fast weich.
Diese kleine Köstlichkeit in der Kälte, beim Gehen:
Als würde der Wintermorgen von innen her zum Croissant,
als würdest du selber zu einem Ofen,
einem Haus, einer Zuflucht.
Du gehst sachter,
ganz von Buttergelb durchdrungen,
wie du durch das Blau und Grau wanderst,
das verlöschende Rosa.

Der Tag beginnt,
das Beste hast du schon gehabt.
____


*Ich wünsche euch allen ein gesundes und gesegnetes Jahr 2017!*

