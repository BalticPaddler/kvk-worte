= 50 Jahre Kanu-Vereinigung Kiel
:lang: de

_Geleitwort zur Festschrift_

50 Jahre -- ein halbes Jahrhundert Vereinsbestehen -- ein Ereignis, das ohne den geringsten Zweifel Würdigung verdient.

Der an dieser Stelle übliche Rückblick auf vergangene Ereignisse und Erfolge und der Ausblick auf eine gute Zukunft erfolgt an anderer Stelle in dieser Schrift. Ich möchte stattdessen darüber nachdenken, was einen Verein leben und gesund sein lässt und was ihn in die Zukunft trägt.

Ein Mensch hat mit 50 Jahren üblicherweise sein Leben geregelt. Kinderkrankheiten und Pubertät sind überstanden, die Dinge geregelt, Reife und Erfahrung bestimmen das Handeln. Bei einem Verein von 50 Jahren sieht das ganz ähnlich aus, aber es gibt einige gravierende Unterschiede.

Ein Mensch erfährt sein gesamtes eigenes Leben -- er sammelt also "Erfahrung". Diese ist die Grundlage für ein immer reiferes Handeln, für Charakter und Identität des Individuums.

Solange ein Verein jung ist, wird eine kontinuierliche Reifung und Charakterbildung stattfinden. Man macht gemeinsame Erfahrungen und entwickelt einen eigenen Stil. Zusammen mit dem sich daraus entwickelnden Traditionen formen sie einen Charakter. Die Verbundenheit der Mitglieder damit macht die Identität des Vereins aus.

Im Unterschied zum Individuum hat ein Verein die Erfahrungen, Talente und Charaktereigenschaften vieler Individuen in sich vereint. Dadurch kann die Gemeinschaft Dinge bewegen und ermöglichen, die ein Einzelner nicht schaffen kann. Und ein Verein ist in seiner Lebensspanne nicht grundsätzlich begrenzt, denn ihm fließen immer wieder neue Charaktere und Talente zu.

Damit Erfahrungen ausgeschiedener, ja sogar verstorbener Mitglieder genutzt werden können, ist Begegnung notwendig und Austausch. So kann, was gut war, überzeugen und übernommen werden. Was sich überlebt hat, wird verworfen, und neue Ideen, die bestechen, werden aufgenommen. Dadurch kann die kollektive Erfahrung überleben, kann der Charakter erhalten und gleichzeitig verjüngt werden und die Identität neu gestärkt. Das ist der beste Sinn des Wortes "Tradition".

Ist ein Verein also unsterblich? Und ewig jung?

Mit jedem Mitglied, das den Verein oder den Vorstand verlässt, verlassen Wissen und Erfahrung, verlassen Charakter und Identität den Verein bzw. den Vorstand. Damit sie nicht verloren sind, müssen ihre Erfahrung und ihr Wissen, weitergegeben, müssen die Traditionen gepflegt werden. Auch hierfür sind Begegnung und Austausch notwendig. Funktioniert das nicht, setzt etwas ähnliches ein, was man beim Individuum als Demenz kennt: die Erinnerung an die eigene Geschichte geht verloren, man begeht längst begangene Fehler erneut, man kann nicht aus der Erfahrung schöpfen.

Wenn die Idee, um die sich die Vereinsmitglieder versammelt haben, nicht mehr verfängt, wird man keine neuen Mitglieder mehr gewinnen und bei den noch vorhandenen wird die Verbundenheit schwinden. Wenn das Ideal verloren geht und immer mehr einem Anspruch auf Dienstleistung weicht, wird Frust bei den Kümmerern entstehen und weniger Freiwillige werden sich finden. Vorstandsarbeit wird immer unattraktiver, wenn sie von immer weniger Schultern getragen wird. Das führt zu Resignation und Aufgabe. Der Verein altert.

Wenn Begegnung und Austausch und Erhalt des Ideals die Voraussetzungen für das gedeihliche Überleben und die Zukunft eines Vereins sind, wie sieht es dann damit bei uns aus?

Wenn man in unser Vereinsprogramm blickt, wenn man sich einen Tag vor unsere Bootshalle stellt, wenn man unsere elektronischen Kommunikationskanäle beobachtet, sieht man, das Begegnung und Austausch unseren Vereinsalltag bestimmen. Obwohl wir kaum noch Gründungsmitglieder in unseren Reihen haben, ist es uns durch das rege Miteinander bisher gut gelungen, Erfahrungen zu erhalten und zum Wohle des Vereins zu nutzen. Dabei spielte immer der Ältestenrat und die hohe Anerkennung dieser Einrichtung eine gewichtige Rolle. Während im Vereinsalltag die Weitergabe von Tradition und Erfahrung eher beiläufig stattfinden, stellt der gesamte Prozess, den unser Jubiläum in Gang gesetzt hat, eine geballte Ladung in dieser Richtung dar. Er ist letztlich nichts anderes, als dass die Geschichte des Vereins "vergegenwärtigt" und den jetzigen Mitgliedern weitergegeben wird. 

Beim Ideal unseres Vereins handelt es sich nicht nur um die Begeisterung für den Kanusport. Auch der Umstand, dass man sich überhaupt ehrenamtlich für eine Sache einsetzt und ihr Zeit und Arbeit opfert, zählt dazu. Interesse für den Kanusport wird uns von außen in hinreichendem Maße entgegen gebracht. Es in Begeisterung zu verwandeln und sich für die Sache einzusetzen, wird in unserem Verein von engagierten Mitgliedern insbesondere im Vorstand in bewundernswerter Weise vorgelebt --- trotz widriger Entwicklung der gesellschaftlicher Rahmenbedingungen.

So ist unser Verein mit seinen 50 Jahren jung und kraftvoll und gesund. Ich werde mich hüten, hier den Bestand für die nächsten 50 Jahre zu beschwören. Aber wir haben alle Voraussetzungen, weitere Jubiläen zu bestehen -- und es liegt an uns, das Leben unseres Vereins jeden Tag wieder neu zu gewinnen und seine Zukunft zu sichern! 

Mathias Weber
1.Vorsitzender
Altenholz, im Juli 2016

