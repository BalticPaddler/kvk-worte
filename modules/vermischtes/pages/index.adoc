= Vermischtes
Mathias-H. Weber <mhw@teambaltic.de>
v1.0
:doctype: book
:encoding: utf-8
:lang: de
//:toc: left
//:toclevels: 4
//:toc-title: Inhaltsverzeichnis
:last-update-label: Erstellt mit Asciidoctor v{asciidoctor-version} : Zuletzt geändert:
:icons: font
:numbered:
:source-highlighter: highlightjs

    ** xref:vermischtes:10-jahre-rst.adoc[10 Jahre Regionales Sicherheitstrainung]
    ** xref:vermischtes:metatourbericht.adoc[Meta-Tourbericht]
    ** xref:vermischtes:warum-vortraege.adoc[Warum Vorträge?]
    ** xref:vermischtes:flaschenpost.adoc[Flaschenpost]

