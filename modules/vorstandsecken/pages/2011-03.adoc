= Vorstandsecke
Mathias-H. Weber
:lang: de
:sectlinks:
// Makro "kbd:" aktivieren:
:experimental:

== März 2011

Vor einem guten Jahr ist ein lieber Freund von mir gestorben. Er hatte sich immer sehr für die Gemeinschaft eingesetzt und war darin ein großes Vorbild für mich. Der Leitspruch, den er sich selbst für seine Beerdigung ausgesucht hat, drückt aus, wie selbstverständlich er den Einsatz für die Gemeinschaft sah:

	"Dienet einander -- ein jeder mit der Gabe, die er empfangen hat."
	
Als es im Vorfeld zur Suche nach einem neuen Ersten Vorsitzenden darum ging, ob ich die Aufgabe übernehmen würde, ging mir dieser Spruch häufig durch den Sinn. Dass ich die Gabe empfangen hätte, den Verein führen zu können, schien anderen viel klarer zu sein als mir selbst. Es hat viele Gespräche gegeben, die mir gezeigt haben, wie viele Menschen es gibt, die diesen Verein schätzen und ihm einen hohen Wert beimessen. In meiner Unsicherheit, ob ich der Aufgabe gewachsen sein würde, ist mir viel Unterstützung zugesagt worden: "Wir machen es, so gut wir es können!" Danke, für diesen Trost, Hela, und Danke für das "Wir" in diesem Satz. Danke allen, die mich im Vorfeld darin bestärkt haben, die Aufgabe zu übernehmen. Und ich möchte Klaus-Peter danken. Dafür, dass er den Verein so lange und durch manchmal schwierige Zeiten bis hierher geführt hat. Mein Dank gilt auch allen, die sich auf der Mitgliederversammlung bereit erklärt haben, ein Amt zu übernehmen und sich mit mir für den Vereines einzusetzen. Hier möchte ich besonders die hervorheben, die neu in den Vorstand gekommen sind.

Ich freue mich auf die Aufgabe und möchte alle einladen, daran mitzuarbeiten, unseren tollen Verein weiter nicht nur am Leben zu erhalten, sondern ihn mit Leben zu erfüllen. Die große Geschlossenheit bei der Wahl hat gezeigt, dass wir alle ein gemeinsames Interesse haben und uns dessen auch bewusst sind. Aber auch wenn wir alle in einem Boot sitzen, können wir nur dann einen stabilen Kurs fahren, wenn wir eben nicht alle auf der gleichen Seite stehen. Ich bin mehr als bereit, mit allen zusammenzuarbeiten und alle anzuhören. Ich werde nie allen gerecht werden, diesen Anspruch habe ich gar nicht. Aber wo Skepsis herrscht, müssen wir versuchen, diese Skepsis herauszuarbeiten und zu unserem Nutzen zu wenden. Ich glaube, dass wir auf dem Wege der gemeinsamen Arbeit Trennendes überwinden können und das Vereinende finden.

Und es gibt genug Arbeit in der nächsten Zeit. Der Vorstand wird dabei vorangehen, aber natürlich ist er auf eure Mitarbeit angewiesen. Die Vergangenheit hat gezeigt, dass die Bereitschaft mit anzupacken in hohem Maße vorhanden ist. Wir werden versuchen, Hürden, die manchmal verhindern, dass sich hier noch mehr entfaltet, weiter abzubauen.

Ich sehe es als meine vordringliche Aufgabe an, die von vielen Helfern mit hohem Einsatz vorangetriebene Planung für den Ausbau möglichst zügig in die Realität umzusetzen. Damit wir wieder Anschluss an den Standard der anderen Verein erhalten und uns eine zeitgemäße Ausstattung im sanitären Bereich attraktiv für Neuzugänge macht.

Wir müssen versuchen, sowohl im Polo-Bereich wie auch beim Wanderpaddeln unsere Gruppen und Mannschaften zu festigen und weiter auszubauen. Wenn in der Vergangenheit manches vielleicht nicht wie gewünscht verlaufen ist, sind wir nun aufgefordert, uns auf unsere Stärken zu besinnen und sie weiter zu fördern. Und wir haben beachtliche Stärken, auf die wir stolz sein können!

So wie die Dinge liegen, wird Maja die Bewirtung in unseren Räumen zum nächsten Jahr aufgeben. Sie ist genauso lange im Verein wie ich und dieser Schritt fällt ihr schwer, aber persönliche Gründe zwingen sie dazu. Ich verstehe ihre Entscheidung, persönlich bedauere ich sie aber. Doch wir müssen nach vorne blicken und vor dem Hintergrund des allgemeinen Um- und Aufbruches im Verein ist es vielleicht genau der richtige Zeitpunkt für eine Neuorientierung auch in diesem Bereich.

Ich war heute auf dem Wasser und es lässt sich nicht leugnen: Es will Frühling werden! Ich freue mich darauf, ihn mit euch im Bootshaus und auf dem Wasser zu begrüßen!

Mathias Weber +
1. Vorsitzender